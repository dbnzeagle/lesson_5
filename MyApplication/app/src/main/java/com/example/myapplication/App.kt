package com.example.myapplication

import android.app.Application

class App : Application() {
  lateinit var retrofit: Api

  override fun onCreate() {
    super.onCreate()
    retrofit = Network.provideRetrofit()
  }
}
