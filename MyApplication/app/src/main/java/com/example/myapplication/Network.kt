package com.example.myapplication

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Network {
  lateinit var retrofit:Api

  fun provideOkHttp(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    return OkHttpClient.Builder()
      .addInterceptor(logging)
      .connectTimeout(10000, TimeUnit.MILLISECONDS)
      .retryOnConnectionFailure(true)
      .followRedirects(true).build()
  }
  private fun provideGson(): Gson {
    return GsonBuilder().setLenient().create()
  }


  fun provideRetrofit(): Api {
    val baseUrl = "https://test.kode-t.ru"

    val retrofit = Retrofit.Builder().baseUrl(baseUrl).client(provideOkHttp())
      .addConverterFactory(GsonConverterFactory.create(provideGson())).build().create(Api::class.java)
    return retrofit
  }
}
