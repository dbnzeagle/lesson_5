package com.example.myapplication

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import java.util.concurrent.TimeUnit

class MyManager(context: Context, workerParams: WorkerParameters) :
  Worker(context, workerParams) {


  override fun doWork(): Result {
    try {
      Log.d("Start", "doWork: start")
      TimeUnit.SECONDS.sleep(10)
    } catch (e: Throwable) {
      Log.i("Err", e.message.toString())
    }
    Log.d("Stop", "doWork: end")
    return Result.success()
  }

}
