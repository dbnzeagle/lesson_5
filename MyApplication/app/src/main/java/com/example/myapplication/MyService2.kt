package com.example.myapplication

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import java.util.concurrent.TimeUnit

class MyService2 : Service() {

  override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
    Log.i("Start service","OK")
    serviceTask()
    return super.onStartCommand(intent, flags, startId)
  }


  override fun onBind(intent: Intent?): IBinder? {
    return null
  }

  fun serviceTask(){
    TimeUnit.SECONDS.sleep(2)
  }
}
