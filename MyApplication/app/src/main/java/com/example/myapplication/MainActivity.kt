package com.example.myapplication

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.annotation.UiThread
import androidx.work.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity() {

  lateinit var api: Api
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    api = Network.provideRetrofit()


    val constrains = Constraints.Builder().setRequiredNetworkType(NetworkType.UNMETERED).build()

    val workManager =
      OneTimeWorkRequest.Builder(MyManager::class.java)
        .setConstraints(constrains)
        .build()
    WorkManager.getInstance().enqueue(workManager)

//    createThread()

    button.setOnClickListener {
      startService()
    }


  }




  private fun startService(){
    startActivity(Intent(this,MyService2::class.java))
  }



  private fun fetchData() {
    val url = URL("https://test.kode-t.ru/recipes")
    val connection = url.openConnection()
    try {
      val inputStream = BufferedReader(InputStreamReader(connection.getInputStream()))
      Log.i("Connection", inputStream.readLine())
    } catch (e: Throwable) {
      Log.i("Error", e.toString())
    }
  }


  fun loadImageFromNet(): Bitmap {
    val url = URL("https://images.freeimages.com/images/small-previews/7e9/ladybird-1367182.jpg")
    return BitmapFactory.decodeStream(url.openConnection().getInputStream())
  }

  private fun createThread() {
    Thread(Runnable {
      fetchData()
    }).start()
  }

  private class DownloadTask : AsyncTask<String, Void, Bitmap>() {

    override fun onPreExecute() {
      super.onPreExecute()
      Log.i("New Async Task", "Hi")
    }

    override fun doInBackground(vararg params: String?): Bitmap {
      val url = URL(params[0])
      return BitmapFactory.decodeStream(url.openConnection().getInputStream())
    }

    override fun onPostExecute(result: Bitmap?) {
      Log.i("BITMAP", result?.byteCount.toString())
    }

  }

}
